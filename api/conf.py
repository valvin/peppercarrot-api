BASE_URL = "https://www.peppercarrot.com/0_sources/"
EPISODES_URL = BASE_URL + 'episodes-v1.json'
LANGS_URL = BASE_URL + 'langs.json'

FLASK_DEBUG = True
FLASK_LISTENNING_ADDRESS = '0.0.0.0'
