from api import conf
import requests
import json,re
def download_episodes_data():
    r = requests.get(conf.EPISODES_URL)
    if r.status_code == 200:
        data = json.loads(r.text)
        episodes = []
        r = re.compile('ep([0-9]{2})_(.+)')
        for ep in data:
            m = r.search(ep['name'])
            if m:
                episode = {}
                episode['id'] = int(m.group(1))
                episode['long_id'] = ep['name']
                episode['name'] = m.group(2).replace('-', ' ')
                episode['translated_languages'] = ep['translated_languages']
                episode['pages'] = ep['pages']
                episode['total_pages'] = ep['total_pages']
                episodes.append(episode)
        return episodes
    else:
        return {'Error while getting episode.json'}


def get_episode(episode_id):
    episodes = download_episodes_data()
    for ep in episodes:
        if ep['id'] == episode_id:
            return ep
    return None


def get_episode_by_lang(lang):
    episodes = download_episodes_data()
    result = []
    for ep in episodes:
        for l in ep['translated_languages']:
            if l == lang:
                result.append(ep)
    return result


def get_episode_files(episode_id, locale, hi_res=False):
    resolution = 'low-res'
    if hi_res:
        resolution = 'hi-res'

    episode = get_episode(episode_id)

    if episode:
        episode_url = conf.BASE_URL + episode['long_id'] + '/' + resolution + '/' + locale + '_'
        files_url = {}
        for file_type, file_name in episode['pages'].items():
            files_url[file_type] = episode_url + file_name
        return files_url
    else:
        return None


