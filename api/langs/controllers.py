from api import conf
import requests
import json


def get_langs_data():

    r = requests.get(conf.LANGS_URL)
    if r.status_code == 200:
        data = json.loads(r.text)
        langs = []
        print(type(data))
        for k, l in data.items():
            lang = {}
            lang['id'] = k
            lang['iso_code'] = l['iso_code']
            lang['iso_version'] = l['iso_version']
            lang['name'] = l['name']
            lang['local_name'] = l['local_name']
            lang['translators'] = l['translators']
            langs.append(lang)
        return langs
    else:
        return {'Error while getting langs.json'}


def get_lang(id):
    langs = get_langs_data()
    for lang in langs:
        if lang['id'] == id:
            return lang
    else:
        return None


def get_lang_by_iso(iso_code):
    langs = get_langs_data()
    for lang in langs:
        if lang['iso_code'] == iso_code:
            return lang
    return None
