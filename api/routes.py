from api import app
from flask import jsonify, request
from api.episodes import controllers as episodes
from api.langs import controllers as langs

# '/' just display a welcome message and a link to the project
@app.route("/")
def r_index():
    return 'Welcome to Pepper&Carrot API more information here : https://framagit.org/valvin/peppercarrot-api'

# '/espisodes/' return all information of all episodes
# it is possible to filter only on a specific language
# using '/episodes/?lang=<lang_id>'
# <lang_id> : language ISO639 two-digits id
@app.route("/episodes/")
def r_get_episodes():
    episodes_list = episodes.download_episodes_data()
    lang = request.args.get('lang')
    if lang :
        return jsonify(episodes.get_episode_by_lang(lang))
    else :
        return jsonify(episodes_list)

# '/episodes/<episode_id>' returns all information about the specified episode
# <episode_id> : id of the episode
@app.route("/episodes/<int:episode_id>")
def r_get_episode(episode_id):
    episode = episodes.get_episode(episode_id)
    if episode:
        return jsonify(episode)
    else:
        return jsonify('Not found')


# '/episodes/<episode_id>/<language_id>' returns episode information and images
# links for the specifed episode. It is possible to choose resolution which set
# by default to 'low' with this syntax
# '/episodes/<episode_id>/<language_id>?resolution=<low|high>'
# <episode_id> : id of the episode
# <lang_id> : language ISO639 two-digits id
@app.route("/episodes/<int:episode_id>/<string:locale>")
def r_get_episode_localized(episode_id, locale):
    episode = episodes.get_episode(episode_id)
    resolution = request.args.get('resolution')
    if episode:
        if locale in episode['translated_languages']:
            episode['locale'] = locale
            if resolution == 'high':
                episode['files'] = episodes.get_episode_files(episode_id, locale, True)
            else:
                episode['files'] = episodes.get_episode_files(episode_id, locale, False)
            episode.pop('translated_languages', None)
            return jsonify(episode)
        else:
            return jsonify('This episode is not translated in that language')
    else:
        return jsonify('Not found')


# '/langs' return all information about already translated language
# it is possible to get a lang using its iso_code with this syntax
# '/langs?iso_code=<iso_code>'
# <iso_code> : language iso code
@app.route("/langs/")
def r_get_langs():
    langs_list = langs.get_langs_data()
    iso_code = request.args.get('iso_code')
    if iso_code:
        return jsonify(langs.get_lang_by_iso(iso_code))
    else:
        return jsonify(langs_list)


# '/langs/<lang_id>' returns all information about the specified language
# <lang_id> : ISO639 two-digit language code
@app.route("/langs/<id>")
def r_get_lang(id):
    lang = langs.get_lang(id)
    return jsonify(lang)

