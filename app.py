
from api import app,conf
if __name__ == '__main__':
    print('Starting Pepper&Carrot API with debug {0}'.format(str(conf.FLASK_DEBUG)))
    app.debug = conf.FLASK_DEBUG
    app.run(host=conf.FLASK_LISTENNING_ADDRESS)

