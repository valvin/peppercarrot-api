# Peppercarrot API

This simple API exposes episodes information based on data located on the official website

## How to use

### Pre-requisities

it requires Flask : `pip install Flask`

### Run server

```
python3 ./app.py
```

### Routes

* http://localhost:5000/episodes
* http://localhost:5000/episodes/15
* http://localhost:5000/episodes/15/fr
* http://localhost:5000/episodes/15/fr?resolution=hight
* http://localhost:5000/langs
* http://localhost:5000/langs/fr
* http://localhost:5000/langs/?iso_code=ko

![](https://framagit.org/valvin/peppercarrot-api/raw/master/doc/peppercarrot-api.gif)
